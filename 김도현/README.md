### kimdohyoun79


## ✅ 체크리스트

- [x] AWS EC2에 Docker 배포
- [x] Swagger 를 이용하여 상품 수정 API 문서 자동화
- [x] Gitlab CI & Crontab CD
- [x] 회원가입, 로그인 구현
- [x] Spring Security 인증 / 인가 필터 구현
- [x] 포스트 작성, 수정, 삭제, 리스트

<br>

---